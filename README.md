# Semi-Stream Similarity Join

## Algorithms

### Trie Join based

### BiTrieJoin based

### Pass-Join based



## Run the program

1. download the source code
2. enter into the source code dicretory and execute "mvn package"
3. execute "cp task.sh.template task.sh;./task.sh"

An example of command should be like this:

>java -Xmx7G -cp  target/semistreamSimilarityJoin-1.0-SNAPSHOT-jar-with-dependencies.jar \
>
>semistreamSimilarityJoin.SemiStreamJoinMeasurement \
>
>--testTime=5 \
>
>--speed=50 \
>
>--levenshteinDistance=1 \
>
>--report=report.csv \
>
>--joinThreadNumber=4
 
Just use paratmeter "help" to get help:

>java -Xmx7G -cp  target/semistreamSimilarityJoin-1.0-SNAPSHOT-jar-with-dependencies.jar \
>
>semistreamSimilarityJoin.SemiStreamJoinMeasurement --help\

## The implementation of parallel join

In the join phrase, the structure of the trie is constant, so a parallel version of join is possible. It potentially
 improve the overall performance, since the algorithm is compute-intensive and the join phrase is the most 
 time consuming part.
 
 
To implement this, two conditions should be considered besides implementing the join method in a thread-safe way. 
If we use multiple threads to execute join, the disk tuples are a critical section. So we need to control the concurrent
access of the disk tuples. On the other hand, we should coordinate the execution of the threads. Briefly, when the
master thread get a new set of disk tuples, it informs the slave threads to execute join; after every slave thread has
done the join, they inform the master thread to execute the prune operation.
