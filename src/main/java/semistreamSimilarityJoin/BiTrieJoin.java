package semistreamSimilarityJoin;

import java.util.*;

/**
 * Use two trie join to improve the processing speed
 */
public class BiTrieJoin <S> implements SimilarityJoin<S>{
    final TrieJoin<S> leftTrie;
    final TrieJoin<S> rightTrie;

    public BiTrieJoin(int split, int maxWordLength){
        leftTrie = new TrieJoin<>(split, maxWordLength, TrieType.LEFT);
        rightTrie = new TrieJoin<>(split, maxWordLength, TrieType.RIGHT);
    }

    public BiTrieJoin(int split) {
        this(split, 31);
    }

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     * If the length of the key is greater than <tt>maxWordLength</tt>,
     * the key will be truncated.
     *
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return value which is the same with the parameter <tt>value</tt>
     */
    @Override
    public void put(String key, S value) {
        leftTrie.put(key, value);
        rightTrie.put(inverseString(key), value);
    }

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     *
     * @param pair a pair of key and value
     */
    @Override
    public void put(Pair<String, S> pair) {
        put(pair.getFirst(), pair.getSecond());
    }

    /**
     * Find a set of keys within the similarity from 0, inclusive, to <tt>maxDistance</tt>, inclusive.
     * If the length of the key is greater than <tt>maxWordLength</tt>, the key will be truncated.*
     * Results from two tries are merged.
     *
     * @param key         a string between which and keys in the trie, the similarities are computed.
     * @param maxDistance only keys in the trie with less than or equal edit distance of <tt>maxDistance</tt>
     *                    for the parameter <tt>key</tt>are seen similar
     * @return similar keys satisfying the <tt>maxDistance</tt> requirement and related values.
     */
    @Override
    public List<MatchResult<S>> get(String key, int maxDistance) {
        List<MatchResult<S>> left = leftTrie.get(key, maxDistance);
        List<MatchResult<S>> right = rightTrie.get(inverseString(key), maxDistance);
        //any trie returns an empty set
        if (left.isEmpty()) {
            return right;
        }
        if (right.isEmpty()) {
            return left;
        }

        //common routine for merge the lists.
        Set<String> leftKeys = new HashSet<>();
        for (MatchResult<S> matchResult: left) {
            leftKeys.add(matchResult.getKey());
        }
        for (MatchResult<S> matchResult: right) {
            String rightKey = matchResult.getKey();
            if (!leftKeys.contains(rightKey)) {
                left.add(new MatchResult<S>(rightKey, matchResult.getValues()));
            }
        }
        return left;
    }
    /**
     * Once a pair of key and value has traversed all the tuples on disk, the pair will be removed in the trie. If a node
     * has no values and no children, it will be removed. The action of removing a node may trigger a recursive deletions
     * of the ancestor nodes of the deleted node to make sure there is no redundant node in the trie.
     *
     * @return the number of deleted pairs fo key and value.
     */
    @Override
    public int prune() {
        int left = leftTrie.prune();
        rightTrie.prune();
        return left;
    }

    /**
     * @return the number of values stored in the trie.
     */
    @Override
    public int size() {
        return leftTrie.size();
    }

    /**
     * @return the number of nodes in the trie.
     */
    @Override
    public int getNodeNumber() {
        return leftTrie.getNodeNumber() + rightTrie.getNodeNumber();
    }

    /**
     * Remove all the nodes and values in the trie.
     */
    @Override
    public void clear() {
        leftTrie.clear();
        rightTrie.clear();
    }

    /**
     * just for testing
     *
     * @return runtime information
     */
    @Override
    public Map<String, Number> runtimeInformation() {
        return leftTrie.runtimeInformation();
    }

    private String inverseString(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
