package semistreamSimilarityJoin;


import util.TypoGenerator;
import util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A stream of words with manually generated typos. The speed control is implemented based feedback control. So it is
 * very precise.
 */
public class ManualWordStream implements Stream<String> {
    private final BlockingQueue<Pair<String, String>> queue;
    private final TypoGenerator generator;
    private final int speed;
    private Thread task;

    /**
     * Create a stream with given word generating speed.
     * @param speed the speed of the stream
     * @param levenshteinDistance the maximum errors of generated typos in terms of edit distance.
     * @param wordLibraryPath the file of words used for generating words and typos.
     * @throws IOException
     */
    public ManualWordStream(int speed, int levenshteinDistance, String wordLibraryPath) throws IOException {
        queue = new LinkedBlockingQueue<>();
        generator = new TypoGenerator(wordLibraryPath, levenshteinDistance);
        this.speed = speed;
    }

    /**
     * Open a stream and make sure it is ready to serve.
     */
    @Override
    public void open() {
        final int sleepTime = 50;
        task = new Thread(() -> {
            try {
                int bunchSize = 5;
                long counter = 0;
                long lastCounter = 0;
                long startingTime = 1;
                long lastStartingTime = 0;
                long rate = 2;
                while (true) {
                    lastStartingTime = startingTime;
                    startingTime = System.currentTimeMillis();
                    if (Thread.currentThread().isInterrupted()) {
                        System.err.println("Manual word stream has been interrupted.");
                        return;
                    }
                    rate = ((counter - lastCounter) * 1000)/ (startingTime - lastStartingTime);
                    if (rate > speed) {
                        bunchSize--;
                    } else if (rate < speed){
                        bunchSize++;
                    }
                    for (int i = 0; i < bunchSize; i++) {
                        if (!queue.offer(generator.generate())) {
                            System.err.println("[Warn]" + Util.timestampString() + ": Congestion Happened.");
                        }
                    }
                    lastCounter = counter;
                    counter += bunchSize;
                    Thread.sleep(sleepTime);
                }
            } catch (InterruptedException e) {
                System.err.println("Manual word stream has been interrupted.");
                return;
            }
        });
        task.start();
    }

    /**
     * Read all the tuples available. This method is asynchronous and return the available data instantly.
     * This method may throw {code UncheckedIOException}
     *
     * @return a list of tuples including key and other information in a tuple.
     */
    @Override
    public List<Pair<String, String>> read() throws IOException{
        return read(0);
    }

    /**
     * Read at most n tuples available. If n is less than or equal to 0, this parameter will be ignored and all available
     * tuples will be returned.
     * @param n maximum number of tuples returned.
     * @return a list of tuples including key and other information in a tuple.
     */
    @Override
    public List<Pair<String, String>> read(int n) throws IOException{
        if (!available() && !task.isAlive()) {
            new IOException("Try to read from a closed stream");
        }
        final int maxSize = n > 0 ? n : Integer.MAX_VALUE;
        List<Pair<String, String>> list = new ArrayList<>();
        Pair<String, String> pair;
        while (!queue.isEmpty() && list.size() < maxSize) {
            pair = queue.poll();
            if (pair != null) {
                list.add(pair);
            } else {
                break;
            }
        }
        return list;
    }

    /**
     * Test whether there are tuples ready to read.
     * This method may throw {code UncheckedIOException}
     *
     * @return if there is available tuples in the stream return true, otherwise return false.
     */
    @Override
    public boolean available() {
        return !queue.isEmpty();
    }

    /**
     * Test whether the steam is able to generate more tuples. If this method return false, the
     * available() method may return true, because some tuples may be stored in the stream's buffer.
     * @return return if the thread generating tuples is still alive.
     */
    @Override
    public boolean isAlive() {
        return task != null && task.isAlive();
    }

    /**
     * Return the number of available tuples in the stream at the time of this method called.
     * The following read() may get more tuples as new tuples may come in the stream during
     * the interval of the two methods.
     * This method may throw {code UncheckedIOException}
     *
     * @return the number of available tuples in the stream.
     */
    @Override
    public int size() {
        return queue.size();
    }

    /**
     * Stop the service and release used resources.
     */
    @Override
    public void close() {
        task.interrupt();
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        Stream<String> typoStream = new ManualWordStream(10, 1, "words.txt");
        typoStream.open();
        int count = 0;
        int size = 0;
        long time = 1;
        long lastTime = 0;
        while (true) {
            lastTime = time;
            time = System.currentTimeMillis();
            size = typoStream.size();
            count += size;
            System.err.println(count + ", " + size * 1000/(time - lastTime));
            List<Pair<String, String>> list = typoStream.read();
            Thread.sleep(1000);
        }
    }
}
