package semistreamSimilarityJoin;

import util.Util;

import java.util.*;


/**
 * This class uses edit distance between strings as the measure of similarity.
 * A trie structure is used to facilitate the computation of the edit distance between one string and a group strings.
 * The trie stores relationships between keys and values, and behaves like a multimap that can store multiple values for
 * one key.
 */
public class TrieJoin<S> implements SimilarityJoin<S>{
    private int size = 0;
    private int nodeNumber = 0;
    private int counter = 0;
    private final Node root;
    private final Deque<List<Pair<Node, ValueWrapper>>> queue;
    
    private final int maxWordLength;
    //the memory pool for storing the table that records the edit distance.
    private final HashMap<Long, Integer[][]> tablePool;

    //state for bitrie join
    private final TrieType trieType;

    /**
     * construct the trie and the queue recording the order of stream tuples.
     * @param split The number of parts the relationship on disk is split to. If we feed one part into the trie every time
     *              , the match between the stream tuples and disk tuples has finished after <tt>split</tt> times.
     * @param maxWordLength  the maximum word length. If the word queried or put into the tire is longer than this value
     *                       the word will be truncated.
     */
    public TrieJoin(int split, int maxWordLength) {
        this(split, maxWordLength, TrieType.NORMAL);
    }

    /**
     * construct the trie and the queue recording the order of stream tuples.
     * @param split The number of parts the relationship on disk is split to. If we feed one part into the trie every time
     *              , the match between the stream tuples and disk tuples has finished after <tt>split</tt> times.
     * @param maxWordLength  the maximum word length. If the word queried or put into the tire is longer than this value
     *                       the word will be truncated.
     * @param trieType used for bitrie join to indicate the type of tries.
     */
    public TrieJoin(int split, int maxWordLength, TrieType trieType) {
        this.trieType = trieType;
        root = new Node();
        nodeNumber = 1;
        this.maxWordLength = maxWordLength;
        tablePool = new HashMap<>();

        //initial queue
        queue = new LinkedList<>();
        for(int i = 0; i < split; i++) {
            queue.add(new ArrayList<>());
        }
    }

    public TrieJoin(int split) {
        this(split, 31);
    }
    
    

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     * If the length of the key is greater than <tt>maxWordLength</tt>,
     * the key will be truncated.
     * @param key key with which the specified value is to be associated
     * @param value  value to be associated with the specified key
     * @return value which is the same with the parameter <tt>value</tt>
     */
    public void put(String key, final S value) {
        if (key.length() > maxWordLength) {
            key = key.substring(0, maxWordLength);
        }
        counter += 1;
        Node node = root;
        Node child = null;
        for (Character c: key.toCharArray()) {
            if (!node.table.containsKey(c)) {
                child = new Node(c);
                nodeNumber += 1;
                child.parent = node;
                node.table.put(c, child);
            } else {
                child = node.table.get(c);
            }
            node = child;
        }

        ValueWrapper wrapper = node.container.add(value);
        //register with the counter
        queue.peekLast().add(new Pair(node, wrapper));
        size += 1;
    }

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     * @param pair a pair of key and value
     */
    public void put(Pair<String, S> pair) {
        put(pair.getFirst(), pair.getSecond());
    }

    /**
     * Find a set of keys within the similarity from 0, inclusive, to <tt>maxDistance</tt>, inclusive.
     * If the length of the key is greater than <tt>maxWordLength</tt>, the key will be truncated.*
     * To accelerate the fuzz search speed, a dynamic programming is employed.
     * @param key a string between which and keys in the trie, the similarities are computed.
     * @param maxDistance only keys in the trie with less than or equal edit distance of <tt>maxDistance</tt>
     *                    for the parameter <tt>key</tt>are seen similar
     * @return similar keys satisfying the <tt>maxDistance</tt> requirement and related values.
     */
    @Override
    public List<MatchResult<S>> get(String key, final int maxDistance) {
        if (key.length() > maxWordLength) {
            key = key.substring(0, maxWordLength);
        }
        //get a table for storing edit distances
        Integer[][] table = getTable();
        List<MatchResult<S>> results = new ArrayList<>();
        Integer[] currentRow = table[0];
        int rightBound = 0;
        for (int i = 0; i <= key.length() && i <= maxDistance; i++) {
            currentRow[i] = i;
            rightBound = i;
        }
        int leftBound = 0;

        for(Map.Entry<Character, Node> entry: root.table.entrySet()) {
            search(entry.getKey(), entry.getValue(), key, table, 1, leftBound, rightBound, results, maxDistance);
        }
        return results;
    }

    /**
     * Remove all the nodes and values in the trie.
     */
    @Override
    public void clear() {
        root.table = new TreeMap<Character, Node>();
        size = 0;
    }

    /**
     * @return the number of values stored in the trie.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @return the number of nodes in the trie.
     */
    @Override
    public int getNodeNumber() {
        return nodeNumber;
    }


    /**
     * Once a pair of key and value has traversed all the tuples on disk, the pair will be removed in the trie. If a node
     * has no values and no children, it will be removed. The action of removing a node may trigger a recursive deletions
     * of the ancestor nodes of the deleted node to make sure there is no redundant node in the trie.
     * @return the number of deleted pairs fo key and value.
     */
    @Override
    public int prune() {
        final List<Pair<Node, ValueWrapper>> removingList = queue.poll();
        for (Pair<Node, ValueWrapper> p: removingList) {
            Node node = p.getFirst();
            ValueWrapper wrapper = p.getSecond();
            //remove value from the container
            node.container.remove(wrapper);

            tryToRemoveNode(node);
        }
        int removedSize = removingList.size();
        removingList.clear();
        queue.add(removingList);  //reuse the list
        size -= removedSize;
        return removedSize;
    }

    /**
     * If a node has no children and its container is empty, remove the node. If the node is deleted, apply the same
     * process to its parent.
     */
    private void tryToRemoveNode(Node node) {
        while(node.table.isEmpty() && node.container.isEmpty() && node != root) {
            Character v = node.value;
            node = node.parent;
            node.table.remove(v);
            nodeNumber -= 1;
        }
    }

    /**
     * Use the depth first traverse to recursively search the similar words.
     */
    private void search(final Character c, final Node node, final String key, final Integer[][] table,
                        final int depth, final int leftBound, final int rightBound, 
                        List<MatchResult<S>> results, final int maxDistance) {
        final int currentMaxDistance = getCurrentMaxDistance(key.length(), maxDistance, depth);

        Integer[] previousRow = table[depth - 1];
        Integer[] currentRow = table[depth];
        currentRow[leftBound] = previousRow[leftBound] + 1;
        int newLeftBound = currentRow[leftBound] <= currentMaxDistance ? leftBound : -1;
        int newRightBound = -1;
        for (int column = leftBound + 1; column <= rightBound + 1 && column <= key.length(); column++) {
            int insertCost = currentRow[column - 1] + 1;
            //caution: the value of previousRow[rightBound + 1] is more than max distance.
            int deleteCost = column == rightBound + 1 ? Integer.MAX_VALUE : previousRow[column] + 1;

            int replaceCost;
            if (key.charAt(column - 1) != c) {
                //substitution distance gets the value 1 is more common than 2, although the original paper assigned it to 2 
                replaceCost = previousRow[column - 1] + 1;  
            } else {
                replaceCost = previousRow[column - 1];
            }
            currentRow[column] = Integer.min(insertCost, Integer.min(deleteCost, replaceCost));
            if (currentRow[column] <= currentMaxDistance) {
                if (newLeftBound == -1) {
                    newLeftBound = column;
                }
                newRightBound =  column;
            }
        }

        //store found matches
        if (newRightBound == key.length() && !node.container.isEmpty()) {
            String path = getPath(node);
            List<S> values = new ArrayList<>();
            for(ValueWrapper v = node.container.header.next; v != null; v = v.next) {
                values.add(v.value);
            }
            results.add(new MatchResult<S>(path, values));
        }

        if (newLeftBound > -1) {
            for(Map.Entry<Character, Node> entry: node.table.entrySet()) {
                search(entry.getKey(), entry.getValue(), key, table, depth + 1, newLeftBound, newRightBound, results, maxDistance);
            }
        }
    }

    /**
     * Visit upward from the <tt>node</tt> to the root and record the characters in the path.
     * For the right, it returns the inversion of the path
     */
    private String getPath(Node node) {
        StringBuilder s = new StringBuilder();
        for (Node p = node; p != root; p = p.parent) {
            if (trieType == TrieType.RIGHT) {
                s.append(p.value);
            } else {
                s.insert(0, p.value);
            }
        }
        return s.toString();
    }

    /**
     * allocate one matrix for every thread 
     * @return matrix to store edit distances
     */
    private Integer[][] getTable() {
        Long id = Thread.currentThread().getId();
        if (tablePool.containsKey(id)) {
            return tablePool.get(id);
        } else {
            Integer[][] table = new Integer[maxWordLength + 1][maxWordLength + 1];
            tablePool.put(id, table);
            return table;
        }
    }

    private int getCurrentMaxDistance(final int keyLength, final int maxDistance, final int depth) {
        if (trieType == TrieType.NORMAL) {
            return maxDistance;
        } else if (trieType == TrieType.LEFT) {
            if (depth <= keyLength/2) {
                return maxDistance/2;
            } else {
                return maxDistance;
            }
        } else { //TrieType.RIGHT
            if (depth <= keyLength - keyLength/2) {
                return maxDistance/2;
            } else {
                return maxDistance;
            }
        }
    }

    /**
     * Inner nodes and leaves used to construct the trie.
     */
    private class Node {
        Map<Character, Node> table;
        Container container;  //contain the values
        Node parent;
        Character value;

        Node(Character value) {
            this.value = value;
            table = new TreeMap<Character, Node>();  //TODO: we maybe get a better version if we replace TreeMap with bitmap 
            this.container = new Container();
        }

        Node() { this(null);}
    }

    /**
     * It contains values in a trie. The inner structure is a double-linked list.
     */
    private class Container {
        final ValueWrapper header;
        Container() {
            header = new ValueWrapper(null);
        }

        ValueWrapper add(final S value) {
            ValueWrapper valueWrapper = new ValueWrapper(value);
            valueWrapper.previous = header;
            valueWrapper.next = header.next;
            if (header.next != null) {
                header.next.previous = valueWrapper;
            }
            header.next = valueWrapper;
            return valueWrapper;
        }

        ValueWrapper remove(final ValueWrapper valueWrapper) {
            valueWrapper.previous.next = valueWrapper.next;
            if (valueWrapper.next != null) {
                valueWrapper.next.previous = valueWrapper.previous;
            }
            //safe guard
            valueWrapper.previous = null;
            valueWrapper.next = null;
            return valueWrapper;
        }

        boolean isEmpty() {
            return header.next == null;
        }

    }

    /**
     * The Element constructing the linked list {@code Container}.
     * The attributes of elements are also publicly accessed by the queue recording the order of the stream tuples,
     * so it is encapsulated in the {@code Container}.
     */
    private class ValueWrapper {
        S value;
        ValueWrapper previous;
        ValueWrapper next;

        ValueWrapper(S value) {
            this.value = value;
        }
    }

    /**
     * just for testing
     * @return runtime information
     */
    @Override
    public Map<String, Number> runtimeInformation() {
        Map<String, Number> stat= new HashMap<>();

        Runtime runtime = Runtime.getRuntime();
        long memory = (runtime.totalMemory() - runtime.freeMemory())/1024;

        stat.put("counter", this.counter);
        stat.put("trieNodeNumber", this.nodeNumber);
        stat.put("trieSize", this.size());
        stat.put("memory", memory);
        return stat;
    }
}
