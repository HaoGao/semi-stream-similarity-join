package semistreamSimilarityJoin;


import org.apache.commons.cli.*;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import util.Util;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiConsumer;


/**
 * This program use manually generated stream including random words and typos to join with disk relationship.
 * All parameters are optional.
 * usage:
 * --speed: tuple arrival speed. The default value is 50.
 * --split: the number of partitions the disk relationship is split into. The default value is 10.
 * --levenshteinDistance: the maximum errors occur in one typo. The default value is 1.
 * --testTime: this program will quit after this time. The unit is second. The default value is infinite.
 * --diskFile: the path for the disk relationship file. The default is "relationship.txt"
 * --joinThreadNumber: the number of threads executing join between disk tuples and the trie. The default
 * value is 1. If provided, this parameter should be greater than 0.
 * --autoFlushStatisticResult: The default value is False.
 * --report: the path of the test report. The default is "report.txt"
 * --trieType: 3 for passjoin, 2 for bitrejoin, others for normal trie join.
 * --outputResults: print results through stdout.
 */
public class SemiStreamJoinMeasurement {
    public static void main(String[] args) throws IOException, ParseException, SQLException, ClassNotFoundException {
        Options options = setBuildOptions();
        final Configuration configuration = parseOptions(getDefaultConfiguration(), options, args);

        if (configuration.containsKey("help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Semi-Stream Join", options);
            System.exit(0);
        }
        
        printJobInformation(configuration);

        //setup the job
        //set stream
        Stream<String> stream;
        if (configuration.containsKey("stream") && configuration.getString("stream").equals("DB")) {
            stream = new DBWordStream("jdbc:mysql://localhost:3306/twitter", "lab", "streamjoin", configuration.getInt("speed"));
        } else {
            stream = new ManualWordStream(configuration.getInt("speed"), 2, configuration.getString("wordLibrary"));
        }
        //set buffer
        Buffer<String> buffer = new ParallelDiskBuffer(configuration.getString("diskFile"),
                configuration.getInt("split"));

        int maxDistance = configuration.getInt("levenshteinDistance");
        Index[] indices = new Index[buffer.getSplit()];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = new PassJoinIndex(maxDistance);
        }

        SimilarityJoin<String> similarityJoin;
        if (configuration.getInt("trieType") == 2 ) {
            System.err.println("BiTrie Join");
            similarityJoin = new BiTrieJoin<String>(buffer.getSplit());
        } else if (configuration.getInt("trieType") == 3) {
            similarityJoin = new FilteringVerificationSimilarityJoin<>(indices, maxDistance, 32);
        } else {
            System.err.println("Norm Trie Join");
            similarityJoin = new TrieJoin<>(buffer.getSplit());
        }

        boolean outputResults = configuration.getBoolean("outputResults");
        BiConsumer<Pair<String, String>, List<MatchResult<String>>> consumer =
                (diskTuple, matchResults) -> {
                    if (outputResults && !matchResults.isEmpty()) {
                        System.out.println(diskTuple + "------------");
                        for (MatchResult<String> r : matchResults) {
                            System.out.println(r.getKey());
                        }
                    }
                };

        //run the job
        ParallelSemiStreamJoin<String, String> semiStreamJoin = new ParallelSemiStreamJoin.Builder<String, String>()
                .setStream(stream)
                .setSimilarityJoin(similarityJoin)
                .setBuffer(buffer)
                .setConsumer(consumer)
                .setMaxDistance(configuration.getInt("levenshteinDistance"))
                .setJoinThreadNumber(configuration.getInt("joinThreadNumber"))
                .build();
        
        monitor(configuration, stream, buffer, similarityJoin, semiStreamJoin);
        
        semiStreamJoin.start();
        System.err.println("System exited.");
    }

    /**
     * just for testing the performance.
     */
    private static void monitor(Configuration configuration, Stream<String> stream, Buffer<String> buffer, 
                                SimilarityJoin<String> similarityJoin,
                                ParallelSemiStreamJoin semiStreamJoin) throws IOException {
        //measure the performance
        boolean autoFlushStatisticResult = configuration.getBoolean("autoFlushStatisticResult");
        long testTime = configuration.getLong("testTime");
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(configuration.getString("report")));
        Thread monitor = new Thread(() -> {
            try {
                long second = 0;
                while (true) {
                    second += 1;
                    Map<String, Number> stat = similarityJoin.runtimeInformation();
                    String info = Util.join(new Object[]{
                            second,
                            stream.size(),
                            stat.get("counter"),
                            stat.get("trieNodeNumber"),
                            stat.get("trieSize"),
                            stat.get("memory"),
                    }, ",");
                    try {

                        if (second == 1) {
                            writer.write(Util.join(new String[]{
                                    "time", "streamBufferSize", "counter", "trieNodeNumber", "trieSize", "memory"
                            }, ",") + "\n");
                        }
                        writer.write(info + "\n");
                        if (autoFlushStatisticResult) {
                            writer.flush();
                        }
                        if (testTime > 0 && second >= testTime) {
                            writer.flush();
                            stream.close();
                            buffer.close();
                            semiStreamJoin.stop();
                            System.err.println("Monitor thread exited");
                            System.exit(0);
                        }
                    } catch (IOException e) {
                        System.err.println("Monitor thread got a exception." + Util.getContentOfStackTrace(e));
                        return;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.err.println("Monitor thread was interrupted" + Util.getContentOfStackTrace(e));
                        return;
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
        monitor.start();
    }

    private static void printJobInformation(Configuration configuration) {
        System.err.println("Job Settings:");
        for (Iterator<String> iterator = configuration.getKeys(); iterator.hasNext();) {
            String key = iterator.next();
            System.err.println(key + ": " + configuration.getProperty(key));
        }
        System.err.println("Job Settings End");
    }

    private static Configuration parseOptions(Configuration configuration, Options options, String[] args) throws ParseException {
        CommandLine commandLine = new GnuParser().parse(options, args);
        for (Option option: commandLine.getOptions()) {
            configuration.setProperty(option.getOpt(), option.getValue());
        }
        return configuration;
    }

    /**
     * set default values for command line options.
     * @return Configuration with default values
     */
    private static Configuration getDefaultConfiguration() {
        Configuration configuration = new BaseConfiguration();
        //set default values
        configuration.setProperty("stream", "Random");
        configuration.setProperty("speed", 50);
        configuration.setProperty("split", 10);
        configuration.setProperty("levenshteinDistance", 1);
        configuration.setProperty("testTime", 0);
        configuration.setProperty("diskFile", "relation.txt");
        configuration.setProperty("joinThreadNumber", 1);
        configuration.setProperty("autoFlushStatisticResult", false);
        configuration.setProperty("report", "report.csv");
        configuration.setProperty("trieType", 1);
        configuration.setProperty("wordLibrary", "words.txt");
        configuration.setProperty("outputResults", false);
        return configuration;
    }

    /**
     * Set command line options
     * @return command line options
     */
    private static Options setBuildOptions() {
        Option helpOption = OptionBuilder.create("help");
        Option streamOption = OptionBuilder.withArgName("Stream[DB/Random:")
                .hasArg()
                .withDescription("specify the stream sources.")
                .create("stream");
        Option speedOption = OptionBuilder.withArgName("Integer")
                .hasArg()
                .withDescription("tuple arrival speed. The default value is 50.")
                .create("speed");
        Option splitOption = OptionBuilder.withArgName("Integer")
                .hasArg()
                .withDescription("the number of partitions the disk relationship is split into. The default value is 10.")
                .create("split");
        Option levenshteinDistance = OptionBuilder.withArgName("Integer")
                .hasArg()
                .withDescription("the maximum errors occur in one typo. The default value is 1.")
                .create("levenshteinDistance");
 
        Option testTimeOption = OptionBuilder.withArgName("Integer")
                .hasArg()
                .withDescription("this program will quit after this time. The unit is second. " +
                        "The default value is infinite. The default value is infinite.")
                .create("testTime");

        Option diskFileOption = OptionBuilder.withArgName("File")
                .hasArg()
                .withDescription("the path for the disk relationship file. The default is \"relationship.txt\"")
                .create("diskFile");

        Option joinThreadNumberOption = OptionBuilder.withArgName("Integer")
                .hasArg()
                .withDescription("the number of threads executing join between disk tuples and the trie. The default" +
                        "value is 1. If provided, this parameter should be greater than 0")
                .create("joinThreadNumber");

        Option autoFlushStatisticResultOption = OptionBuilder.withArgName("Boolean")
                .hasArg()
                .withDescription("the default value is false")
                .create("autoFlushStatisticResult");

        Option reportOption = OptionBuilder.withArgName("File")
                .hasArg()
                .withDescription("the path of the test report. The default is \"report.txt\"")
                .create("report");

        Option trieTypeOption = OptionBuilder.withArgName("TrieType:1/2")
                .hasArg()
                .withDescription("The type of the trie join. 2 means bitrie-join")
                .create("trieType");

        Option wordLibraryOption = OptionBuilder.withArgName("File")
                .hasArg()
                .withDescription("the path for the word library. The default is \"words.txt\"")
                .create("wordLibrary");

        Option outputResultsOption = OptionBuilder.withArgName("Boolean")
                .hasArg()
                .withDescription("whether printing the results through stdout. The fault is false")
                .create("outputResults");

        Options options = new Options()
                .addOption(helpOption)
                .addOption(streamOption)
                .addOption(speedOption)
                .addOption(splitOption)
                .addOption(levenshteinDistance)
                .addOption(testTimeOption)
                .addOption(diskFileOption)
                .addOption(joinThreadNumberOption)
                .addOption(autoFlushStatisticResultOption)
                .addOption(reportOption)
                .addOption(trieTypeOption)
                .addOption(wordLibraryOption)
                .addOption(outputResultsOption);
        return options;
    }
}
