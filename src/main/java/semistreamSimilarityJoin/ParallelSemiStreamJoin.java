package semistreamSimilarityJoin;

import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;

/**
 * coordinate the execution of the stream, disk buffer and join method.
 * @param <S> the type of the value in stream tuples
 * @param <D> the type of the value in disk tuples
 */
public class ParallelSemiStreamJoin<S, D> {
    private final Stream<S> stream;
    private final SimilarityJoin<S> similarityJoin;
    private final Buffer<D> buffer;
    private final BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer;

    //Levenshtein distance, words with a distance less that maxDistance are seen similar words.
    private final int maxDistance;
    private final Thread[] joinTasks;
    private final int joinThreadNumber;
    //for reading a list
    private ListIterator<Pair<String, D>> diskTupleIterator;
    //lock held by reads
    private final ReentrantLock diskTupleReadLock = new ReentrantLock();
    //waiting queue for waiting reads
    private final Condition diskTupleNotEmpty = diskTupleReadLock.newCondition();

    /*
    * used to coordinate the master thread get tuples from a stream and the salve threads executing the join.
    * This barrier should be renewed every loop.
    */
    private CountDownLatch joinDoneSignal;


    private ParallelSemiStreamJoin(Builder builder) {
        stream = builder.stream;
        consumer = builder.consumer;
        similarityJoin = builder.similarityJoin;
        buffer = builder.buffer;
        maxDistance = builder.maxDistance;
        joinThreadNumber = builder.joinThreadNumber;
        joinTasks = new Thread[builder.joinThreadNumber];
        for(int i = 0; i < joinTasks.length; i++) {
            joinTasks[i] = new Thread(this::join);
        };
    }

    /**
     * turn on all working thread and start to work
     * @throws IOException exception may be caused by the Buffer::read
     */
    public void start() throws IOException {
        stream.open();
        for (Thread task: joinTasks) {
            task.start();
        }
        while(stream.isAlive() || stream.available()) {
            if (Thread.currentThread().isInterrupted()) {
                System.err.println("try to close");
                System.err.println("ParallelSemiStreamJoin has stopped");
            }
            List<Pair<String, S>> streamTuples = stream.read();
            streamTuples.forEach(similarityJoin::put);
            setDiskTuples(buffer.read());
            prune();
        }
    }

    private void join() {
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                System.err.println("Jion thread: " + Thread.currentThread().getName() + " has been interrupted");
                return;
            }
            Pair<String, D> diskTuple = read();
            List<MatchResult<S>> result = similarityJoin.get(diskTuple.getFirst(), maxDistance);
            consumer.accept(diskTuple, result);
        }
    }

    private void prune() {
        try {
            joinDoneSignal.await();
        } catch (InterruptedException e) {
            System.err.println("joinDoneSignal was interrupted.");
        }
        similarityJoin.prune();
    }

    /**
     * set source of disk tuples, and signal slave threads to execute join. 
     * @param diskTuples
     */
    private void setDiskTuples(List<Pair<String, D>> diskTuples) {
        this.diskTupleIterator = diskTuples.listIterator();
        joinDoneSignal = new CountDownLatch(joinThreadNumber);
        diskTupleReadLock.lock();
        try {
            diskTupleNotEmpty.signalAll();
        } finally {
            diskTupleReadLock.unlock();
        }
    }

    private Pair<String, D> read() {
        Pair<String, D> tuple = null;
        diskTupleReadLock.lock();
        try {
            while (diskTupleIterator == null || !diskTupleIterator.hasNext()) {
                if (diskTupleIterator != null && !diskTupleIterator.hasNext()) {
                    joinDoneSignal.countDown();
                }
                diskTupleNotEmpty.await();  //only the method setDiskTuples can wake up this wait
            }
            tuple = diskTupleIterator.next();
        } catch (InterruptedException e) {
            //emtpy  TODO: write log
        } finally {
            diskTupleReadLock.unlock();
        }
        return tuple;
    }
    
    public void stop() {
        for (Thread task: joinTasks) {
            task.interrupt();
        }
        Thread.currentThread().interrupt();
    }

    /**
     * A helper class to build a SemiStreamJoin instance.
     * @param <S> the type of the value in stream tuples
     * @param <D> the type of the value in disk tuples
     */
    public static class Builder <S, D> {
        private Stream<S> stream;
        private BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer;
        private SimilarityJoin<S> similarityJoin;
        private Buffer<D> buffer;
        private int maxDistance;
        private int joinThreadNumber = 1;

        /**
         * The only interface to build an instance of SemiStreamJoin.
         * Before calling this method, you should set the components required by the SemiStreamJoin, otherwise this
         * method may fail 
         * @return an SemiStreamJoin instance.
         */
        public ParallelSemiStreamJoin<S, D> build() {
            return new ParallelSemiStreamJoin<>(this);
        }

        public Builder<S, D> setStream(Stream<S> stream) {
            this.stream = stream;
            return this;
        }

        public Builder<S, D> setConsumer(BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer) {
            this.consumer = consumer;
            return this;
        }

        public Builder<S, D> setSimilarityJoin(SimilarityJoin<S> similarityJoin) {
            this.similarityJoin = similarityJoin;
            return this;
        }

        public Builder<S, D> setBuffer(Buffer<D> buffer) {
            this.buffer = buffer;
            return this;
        }

        public Builder<S, D> setMaxDistance(int maxDistance) {
            this.maxDistance = maxDistance;
            return this;
        }

        public Builder<S, D> setJoinThreadNumber(int joinThreadNumber) {
            if (joinThreadNumber <= 0) {
                throw new IllegalArgumentException("The number of thread executing join should be greater than 0");
            }
            this.joinThreadNumber = joinThreadNumber;
            return this;
        }
    }


}
