package semistreamSimilarityJoin;

import java.util.HashMap;

/**
 * Edit distance calculator implementation using dynamic algorithm.
 */
public class EditDistanceCalculator {

    private final HashMap<Long, int[][]> tablePool = new HashMap<>();
    private final int maxWordLenght;

    /*
     * @param maxWordLength specify the max word length
     */
    public EditDistanceCalculator(int maxWordLength) {
        this.maxWordLenght = maxWordLength;
    }

    /**
     * test if the edit distance between a and b is within a specific <tt>maxDistance</tt>
     * if the length of any word is larger than the maxWordLength, the string will be truncated.
     * @param a
     * @param b
     * @param maxDistance
     * @return
     */
    public boolean isSimilar(String a, String b, int maxDistance) {
        Long id = Thread.currentThread().getId();
        int[][] table = tablePool.get(id);
        if (table == null) {
            table = new int[maxWordLenght + 1 ][maxWordLenght + 1];
            tablePool.put(id, table);
        }
        return isSimilar0(a, b, maxDistance, table);
    }


    /**
     *Dynamic algorithm to calculate edit distance
     */
    private boolean isSimilar0(String a, String b, int maxDistance, int[][]table) {
        if (maxDistance < 0) {
            throw new IllegalArgumentException("Maximum distance should be greater than or equal to 0");
        }

        if (maxDistance == 0) {
            return a.equals(b);
        }

        if (a.length() > maxWordLenght) {
            a = a.substring(0, maxWordLenght);
        }

        if (b.length() > maxWordLenght) {
            b = b.substring(0, maxWordLenght);
        }

        table[0][0] = 0;
        //set the first row for b empty
        for (int i = 0; i < a.length(); i++) {
            table[0][i + 1] = i + 1;
        }
        //set the first column for a is empty
        for (int j = 0; j < b.length(); j++) {
            table[j + 1][0] = j + 1;
        }
        int begin = 0;
        int end = Math.min(maxDistance, a.length());
        int newBegin = -1;
        int deleteDistance;
        int insertDistance;
        int replaceDistance;
        int newEnd = -1;
        for (int row = 1; row < b.length() + 1; row++) {  //row is corresponding to the row - 1 letter in b.
            newBegin = -1;
            table[row][begin] = table[row - 1][begin] + 1;
            if (table[row - 1][begin] + 1 <= maxDistance) {
                newBegin = begin;
            }
            for (int col = begin + 1; col <= end + 1 && col <= a.length(); col++) {
                if (a.charAt(col - 1) == b.charAt(row - 1)) {
                    table[row][col] = table[row - 1][col - 1];
                    if (newBegin == -1) {
                        newBegin = col;
                    }
                    newEnd = col;
                } else {
                    replaceDistance = table[row - 1][col - 1] + 1;
                    deleteDistance = table[row][col - 1] + 1;
                    if (col != end + 1) {
                        insertDistance = table[row - 1][col] + 1;
                    } else {
                        insertDistance = Integer.MAX_VALUE;
                    }
                    int min = Math.min(Math.min(replaceDistance, insertDistance), deleteDistance);
                    table[row][col] = min;
                    if (min <= maxDistance) {
                        newEnd = col;
                        if (newBegin == -1) {
                            newBegin = col;
                        }
                    }
                }

            }
            begin = newBegin;
            end = newEnd;
            if (begin == -1) {
                return false;
            }
        }
        if (table[b.length()][a.length()] <= maxDistance) {
            return true;
        } else {
            return false;
        }
    }
}
