package semistreamSimilarityJoin;

import java.util.Comparator;

/**
 * index key for pass join. The object is constant and cannot be changed.
 */
public class IndexKey implements Comparable<IndexKey> {
    public final int length;
    public final int order;
    public final String segment;

    IndexKey(int length, int order, String segment) {
        this.length = length;
        this.order = order;
        this.segment = segment;
    }

    @Override
    public int hashCode() {
        return (segment.length() * 11+ length) * 11 + order;
    }


    @Override
    public boolean equals(Object indexKey) {
        if (this == indexKey) {
            return true;
        }

        if (indexKey == null) {
            return false;
        }

        if (this.getClass() != indexKey.getClass()) {
            return false;
        }

        IndexKey obj = (IndexKey)indexKey;
        return this.length == obj.length && this.order == obj.order && this.segment.equals(obj.segment);
    }

    @Override
    public String toString() {
        return "(" + length + ", " + order + ", " + segment + ")";
    }

    @Override
    public int compareTo(IndexKey o2) {
        if (length != o2.length) {
            return length - o2.length;
        }

        if (order != o2.order) {
            return order - o2.order;
        }

        return segment.compareTo(o2.segment);
    }
}
