package semistreamSimilarityJoin;

/**
 * indicates if this trie is a normal trie, left trie or right trie.
 */
public enum TrieType {
    NORMAL,
    LEFT,
    RIGHT
}
