package semistreamSimilarityJoin;

import util.Util;

import java.util.List;


/**
 * a helper class representing the result of similar keys and related values.
 * @param <T> the type of the values stored in the trie.
 */
public class MatchResult <T> {
    String key;
    List<T> values;

    public MatchResult(String key, List<T> values) {
        this.key = key;
        this.values = values;
    }

    public String getKey() {
        return key;
    }

    public List<T> getValues() {
        return values;
    }

    @Override
    public String toString() {
        StringBuilder builder= new StringBuilder();
        builder.append(key);
        builder.append(": ");
        builder.append(Util.join(values, ","));
        return builder.toString();
    }
}
