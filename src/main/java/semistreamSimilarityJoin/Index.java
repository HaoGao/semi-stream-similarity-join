package semistreamSimilarityJoin;

import java.util.Set;

/**
 * Index interface for filtering-verification join
 * The implementation should behave like a multimap with similarity search function.
 */
public interface Index{
    /**
     * put key-value pair into the index, key can be repeated.
     * @param word
     */
    void put(String word);

    /*
     *find the similar keys and their associated values stored in the index
     */
    Set<String> get(String query, int maxDistance);

    /*
     *The number of values stored in the index
     */
    int size();

    /**
     *The size of the index. For example, if the index is implemented using hash map, this value is the size of the hash
     * map
     */
    int indexSize();

    /*
     *Clear the index
     */
    void clear();
}
