package semistreamSimilarityJoin;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

/**
 * A stream automatically getting tweets from twitter.
 */
public class TwitterWordStream implements Stream <String>{
    private TwitterStream twitterStream;
    private BlockingQueue<Pair<String, String>> queue;
    private boolean isOpen;
    public TwitterWordStream() {
        queue = new LinkedBlockingQueue<>();

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("P1i8YwpHMhQQnwGFPSHw3XzJ9")
                .setOAuthConsumerSecret("Jdhc85wZiWTSI0s0THCHAOo2UIgNKey5INYF6KFzDSVjVcgowS")
                .setOAuthAccessToken("2623222722-oLv4KnfqUFfM0LBnrgIHthCPZiy7JNhmeXsApnS")
                .setOAuthAccessTokenSecret("nJFdybjno19WrLCFuOnc0AHyC6HekjspwquBFKbPfCdsV");
        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        StatusListener listener = new StatusListener() {
            @Override
            public void onStatus(Status status) {
                List<String> words = toEnglishWords(status.getText());
                queue.addAll(words
                                .stream()
                                .map(word -> new Pair<>(word, "@" + status.getUser().getScreenName() + ":" + status.getId()))
                                .collect(Collectors.toList()));
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                //empty
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.err.println("[Twitter]Got track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                //empty
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                //empty
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);
    }
    @Override
    public void open() {
        twitterStream.sample();
        isOpen = true;
    }

    @Override
    public List<Pair<String, String>> read() {
        return read(0);
    }

    @Override
    public List<Pair<String, String>> read(int n) {
        final int maxSize = n > 0 ? n : Integer.MAX_VALUE;
        List<Pair<String, String>> list = new ArrayList<>();
        Pair<String, String> pair;
        while (!queue.isEmpty() && list.size() < maxSize) {
            pair = queue.poll();
            if (pair != null) {
                list.add(pair);
            } else {
                break;
            }
        }
        return list;
    }

    @Override
    public boolean available() {
        return !queue.isEmpty();
    }

    @Override
    public boolean isAlive() {
        //cannot get twitterStream's state, so use an variable to trace it.
        return isOpen;
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public void close() {
        twitterStream.shutdown();
        isOpen = false;
    }

    private List<String> toEnglishWords(String sentence) {
        List<String> words = new ArrayList<>();
        String[] rawWords = sentence.split("\\s+");
        for (String w: rawWords) {
            if (w.startsWith("@")) {
                continue;
            }
            String word = w.toLowerCase();
            if (word.startsWith("#")) {
                word = word.substring(1);
            }
            if (isCharacters(word)) {
                words.add(word);
            }
        }
        return words;
    }

    private boolean isCharacters(String word) {
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (!(c >= 'a' && c <= 'z' || c >= '0' && c <= '9')) {
                return false;
            }
        }
        return true;
    }
}
