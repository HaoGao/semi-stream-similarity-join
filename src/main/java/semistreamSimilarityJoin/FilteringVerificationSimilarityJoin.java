package semistreamSimilarityJoin;

import java.util.*;

/**
 * Filtering-Verification based similarity join
 */
public class FilteringVerificationSimilarityJoin <S> implements SimilarityJoin<S> {
    private final int maxDistance;
    private final Deque<Index> queue;
    private int size = 0;
    private int counter = 0;
    private final EditDistanceCalculator editDistanceCalculator;

    public FilteringVerificationSimilarityJoin(Index[] indices, int maxDistance, int maxWordLength) {
        this.maxDistance = maxDistance;
        queue = new LinkedList<>();
        for (int i = 0; i < indices.length; i++) {
            queue.add(indices[i]);
        }
        editDistanceCalculator = new EditDistanceCalculator(maxWordLength);
    }

    /**
     * Put the key associated with the value into the map.
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return value which is the same with the parameter <tt>value</tt>
     */
    @Override
    public void put(String key, S value) {
        queue.getLast().put(key);
        size += 1;
        counter += 1;
    }

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     *
     * @param pair a pair of key and value
     */
    @Override
    public void put(Pair<String, S> pair) {
        put(pair.getFirst(), null);
    }

    /**
     * Find a set of keys within the similarity from 0, inclusive, to <tt>maxDistance</tt>, inclusive.
     * @param key         a string between which and keys in the trie, the similarities are computed.
     * @param maxDistance dummy parameter, can only be the default maximum edit distance
     * @return similar keys satisfying the <tt>maxDistance</tt> requirement and related values.
     */
    @Override
    public List<MatchResult<S>> get(String key, int maxDistance) {
        Set<String> words = get0(key, this.maxDistance);
        List<MatchResult<S>> results = new ArrayList<>();
        for (String word: words) {
            if (editDistanceCalculator.isSimilar(key, word, this.maxDistance)) {
                results.add(new MatchResult<S>(word, null));
            }
        }
        return results;
    }

    public Set<String> get0(String word, int maxDistance) {
        Set<String> results = new HashSet<>();
        for (Index index: queue) {
            results.addAll(index.get(word, maxDistance));
        }
        return results;
    }

    /**
     * Once a pair of key and value has traversed all the tuples on disk, the pair will be removed.
     * @return the number of deleted pairs fo key and value.
     */
    @Override
    public int prune() {
        Index index = queue.poll();
        int num = index.size();
        size -= num;
        index.clear();
        queue.add(index);
        return num;
    }

    /**
     * It's not supported. For sake of performance, some index implementations such as PassJoinIndex do not support thread-safely
     * getting index size.
     * @throws UnsupportedOperationException
     */
    private int indexSize() {
        throw new UnsupportedOperationException();
    }

    /**
     * @return the number of values stored in the container.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * unsupported method
     */
    @Override
    public int getNodeNumber() {
        throw new UnsupportedOperationException("This method is unsupported for FilteringVerificationSimilarityJoin");
    }

    /**
     * clear all the indices.
     */
    @Override
    public void clear() {
        for (Index index: queue) {
            index.clear();
        }
        counter = 0;

    }

    /**
     * just for testing
     *
     * @return runtime information
     */
    @Override
    public Map<String, Number> runtimeInformation() {
        Map<String, Number> stat= new HashMap<>();

        Runtime runtime = Runtime.getRuntime();
        long memory = (runtime.totalMemory() - runtime.freeMemory())/1024;

        stat.put("counter", this.counter);
        stat.put("trieNodeNumber", 0);
        stat.put("trieSize", size());
        stat.put("memory", memory);
        return stat;
    }


}
