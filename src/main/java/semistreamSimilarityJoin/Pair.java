package semistreamSimilarityJoin;

/**
 * To make the code clear, restrict the use of this class. It is advisable not to use nested Pair. 
 * WARN: not implement the hashCode method, don't use as keys in a table.
 * @param <K> key type
 * @param <V> value type
 */
public class Pair<K, V> {
    private K first;
    private V second;

    public Pair(K first, V second) {
        this.first = first;
        this.second = second;
    }

    public V getSecond() {
        return second;
    }

    public void setSecond(V second) {
        this.second = second;
    }

    public K getFirst() {
        return first;
    }

    public void setFirst(K first) {
        this.first = first;
    }

    @Override
    public String toString() {
        return "(" + first.toString() + ", " + second.toString() + ")";
    }

    @Override
    public boolean equals(Object p) {
        if (this == p) {
            return true;
        }
        if (p == null) {
            return false;
        }
        if (p.getClass() != this.getClass()) {
            return false;
        }
        Pair<?, ?> other = (Pair<?, ?>)p;
        return this.first.equals(other.first) && this.second.equals(other.second);
    }
}
