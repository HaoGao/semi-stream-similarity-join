package semistreamSimilarityJoin;

import java.io.IOException;
import java.util.List;

/**
 * Disk Buffer
 */
public interface Buffer <D> {
    List<Pair<String, D>> read() throws IOException;
    void close() throws IOException;
    int getSplit();
}
