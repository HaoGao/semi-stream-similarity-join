package semistreamSimilarityJoin;

import java.util.List;
import java.util.Map;

/**
 * Created by life on 17/06/15.
 */
public interface SimilarityJoin <S> {
    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     * If the length of the key is greater than <tt>maxWordLength</tt>,
     * the key will be truncated.
     * @param key key with which the specified value is to be associated
     * @param value  value to be associated with the specified key
     * @return value which is the same with the parameter <tt>value</tt>
     */
    void put(String key, final S value);

    /**
     * Put the key associated with the value into the map. If the map
     * previously contained a mapping for the key, the new value is appended.
     * @param pair a pair of key and value
     */
    void put(Pair<String, S> pair);

    /**
     * Find a set of keys within the similarity from 0, inclusive, to <tt>maxDistance</tt>, inclusive.
     * If the length of the key is greater than <tt>maxWordLength</tt>, the key will be truncated.*
     * To accelerate the fuzz search speed, a dynamic programming is employed.
     * @param key a string between which and keys in the trie, the similarities are computed.
     * @param maxDistance only keys in the trie with less than or equal edit distance of <tt>maxDistance</tt>
     *                    for the parameter <tt>key</tt>are seen similar
     * @return similar keys satisfying the <tt>maxDistance</tt> requirement and related values.
     */
    public List<MatchResult<S>> get(String key, final int maxDistance);

    /**
     * Once a pair of key and value has traversed all the tuples on disk, the pair will be removed in the trie. If a node
     * has no values and no children, it will be removed. The action of removing a node may trigger a recursive deletions
     * of the ancestor nodes of the deleted node to make sure there is no redundant node in the trie.
     * @return the number of deleted pairs fo key and value.
     */
    public int prune();

    /**
     * @return the number of values stored in the trie.
     */
    public int size();

    /**
     * @return the number of nodes in the trie.
     */
    public int getNodeNumber();

    /**
     * Remove all the nodes and values in the trie.
     */
    public void clear();

    /**
     * just for testing
     * @return runtime information
     */
    public Map<String, Number> runtimeInformation();


}
