package semistreamSimilarityJoin;

import java.io.IOException;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Plase use {code ParallelSemiStreamJoin}, as this class is depreciated 
 * @param <S> value type in the steam tuple
 * @param <D> value type in the master data tuple
 */
@Deprecated
public class SemiStreamJoin<S, D> {
    private Stream<S> stream;
    private SimilarityJoin<S> similarityJoin;
    private Buffer<D> buffer;
    private BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer;

    private final int maxDistance;

    private SemiStreamJoin(Builder builder) {
        stream = builder.stream;
        consumer = builder.consumer;
        similarityJoin = builder.similarityJoin;
        buffer = builder.buffer;
        maxDistance = builder.maxDistance;

    }

    public void start() throws IOException {
        stream.open();
        while(true) {
            if (stream.isAlive() || stream.available()) {
                List<Pair<String, S>> streamTuples = stream.read();
                streamTuples.forEach(similarityJoin::put);
            }
            List<Pair<String, D>> diskTuples = buffer.read();
            diskTuples.forEach(this::join);
            similarityJoin.prune();
        }
    }

    private void join(Pair<String, D> diskTuple) {
        List<MatchResult<S>> result = similarityJoin.get(diskTuple.getFirst(), maxDistance);
        consumer.accept(diskTuple, result);
    }

    public static class Builder <S, D> {
        private Stream<S> stream;
        private BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer;
        private SimilarityJoin<S> similarityJoin;
        private Buffer<D> buffer;
        private int maxDistance;

        public SemiStreamJoin<S, D> build() {
            return new SemiStreamJoin<>(this);
        }

        public Builder<S, D> setStream(Stream<S> stream) {
            this.stream = stream;
            return this;
        }

        public Builder<S, D> setConsumer(BiConsumer<Pair<String, D>, List<MatchResult<S>>> consumer) {
            this.consumer = consumer;
            return this;
        }

        public Builder<S, D> setSimilarityJoin(SimilarityJoin<S> similarityJoin) {
            this.similarityJoin = similarityJoin;
            return this;
        }

        public Builder<S, D> setBuffer(Buffer<D> buffer) {
            this.buffer = buffer;
            return this;
        }

        public Builder<S, D> setMaxDistance(int maxDistance) {
            this.maxDistance = maxDistance;
            return this;
        }

        /**
         * just an  empty method for keeping consistent with the multi-thread version
         * @param joinThreadNumber
         * @return
         */
        public Builder<S, D> setJoinThreadNumber(int joinThreadNumber) {
            return this;
        }
    }


}
