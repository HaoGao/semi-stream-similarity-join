package semistreamSimilarityJoin;

import java.util.*;

/**
 * Implement passjoin based index
 */
public class PassJoinIndex implements Index {
    private final int maxDistance;
    private final int partition;
    private final Map<IndexKey, Set<String>> map = new HashMap<>();
    private int size = 0;

    /**
     * It creates a passjoin index. For passjoin, the partition of words is based on the maximum distance.
     * @param maxDistance
     */
    public PassJoinIndex(int maxDistance) {
        this.maxDistance = maxDistance;
        partition = maxDistance + 1;
    }

    /**
     * put key-value pair into the index, key can be repeated.
     * @param word
     */
    @Override
    public void put(String word) {
        List<IndexKey> indices = Segmentor.segmentEvenly(word, partition);
        for(IndexKey index: indices) {
            Set set = map.get(index);
            if (set == null) {
                set = new TreeSet<>();
                map.put(index, set);
            }
            set.add(word);
        }
        size += 1;
    }

    /**
     * search a query on the index
     * @param query
     * @param maxDistance for pass-join this value is ignored as the maximum distance is decided when the index is created
     * @return
     */
    @Override
    public Set<String> get(String query, int maxDistance) {
        Set<String> results = new TreeSet<>();
        for (int i = 0; i <= this.maxDistance; i++) {
            List<IndexKey> indices = Segmentor.segmentAllPairs(query, partition, query.length() + i);
            for (IndexKey index: indices) {
                Set<String> words = map.get(index);
                if (words != null) {
                    results.addAll(words);
                }
            }
        }
        return results;
    }

    /**
     * @return the number of words stored in the index.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * The size of the index. For example, if the index is implemented using hash map, this value is the size of the hash
     * map
     */
    @Override
    public int indexSize() {
        return map.size();
    }

    @Override
    public void clear() {
        map.clear();
        size = 0;
    }
}
