package semistreamSimilarityJoin;

import util.Util;

import java.io.IOException;
import java.sql.*;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * A stream reading data from a database
 */
public class DBWordStream implements Stream <String> {
    private final Connection connection;
    private ResultSet result;
    private final BlockingQueue<Pair<String, String>> queue;
    private final int sleepMillis;
    private Thread task;
    private boolean isClosed;


    /**
     * these three parameters are used to connect a database server.
     * @param url url can have the schema's name in the end of the url. 
     *        The format is like "jdbc:mysql://localhost:3306/twitter" where the last part 'twitter' is a schema.
     * @param user account name to connect the database specified by the url.
     * @param password account password to connect the database specified by the url.
     */
    public DBWordStream(String url, String user, String password, int sleepMillis) throws ClassNotFoundException, SQLException {
        //set database

        Class.forName("com.mysql.jdbc.Driver");
        // setup the connection with the DB.
        connection = DriverManager.getConnection(url, user, password);
        queue = new LinkedBlockingDeque<>();
        this.sleepMillis = sleepMillis;
    }
    
    
    /**
     * Open a stream and make sure it is ready to serve.
     * This method may throw {code RuntimeException}
     */
    @Override
    public void open() throws IOException {
        String query = "select id, text from tweet";
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
        } catch (SQLException e) {
            throw new IOException("DBWordStream failed to open", e);
        }
        task = new Thread(()-> {
            try {
                while (result.next()) {
                    if (Thread.currentThread().isInterrupted()) {
                        System.err.println("DBWordStream has been interrupted.");
                    }
                    String id = String.valueOf(result.getLong("id"));
                    String text = String.valueOf(result.getString("text"));
                    String[] words = text.split("\\s+");
                    for (String word: words) {
                        if (!word.isEmpty()) {
                            if (!queue.offer(new Pair<String, String>(word, id))) {
                                System.err.println("[Warn]" + Util.timestampString() + ": Congestion Happened.");
                            }
                        }
                    }
                    Thread.sleep(sleepMillis);
                }
            } catch (SQLException | InterruptedException e) {
                System.err.println("Thread exited caused by an exception." + Util.getContentOfStackTrace(e));
                return;
            } 
        });
        task.start();
        isClosed = false;
    }

    /**
     * Read all the tuples available. This method is asynchronous and return the available data instantly.
     * This method may throw {code UncheckedIOException}
     *
     * @return a list of tuples including key and other information in a tuple.
     */
    @Override
    public List<Pair<String, String>> read() throws IOException {
        return read(0);
    }

    /**
     * Read at most n tuples available. This method may throw {code UncheckedIOException}
     * This method may throw {code UncheckedIOException}
     *
     * @param n maximum number of tuples returned.
     * @return a list of tuples including key and other information in a tuple.
     */
    @Override
    public List<Pair<String, String>> read(int n) throws IOException {
        if (isClosed) {
            throw new IOException("Stream closed");
        }
        final int maxSize = n > 0 ? n : Integer.MAX_VALUE;
        List<Pair<String, String>> list = new ArrayList<>();
        Pair<String, String> pair;
        while (!queue.isEmpty() && list.size() < maxSize) {
            pair = queue.poll();
            if (pair != null) {
                list.add(pair);
            } else {
                break;
            }
        }
        return list;
    }

    /**
     * Test whether there are tuples ready to read.
     * This method may throw {code UncheckedIOException}
     *
     * @return if there is available tuples in the stream return true, otherwise return false.
     */
    @Override
    public boolean available() {
        return !queue.isEmpty();
    }

    /**
     * Test whether the steam is able to generate more tuples. If this method return false, the
     * available() method may return true, because some tuples may be stored in the stream's buffer.
     *
     * @return
     */
    @Override
    public boolean isAlive() {
        try {
            return !result.isAfterLast();
        } catch (SQLException e) {
            //empty
        }
        return false;
    }

    /**
     * Return the number of available tuples in the stream at the time of this method called.
     * The following read() may get more tuples as new tuples may come in the stream during
     * the interval of the two methods.
     *
     * @return the number of available tuples in the stream.
     */
    @Override
    public int size() {
        return queue.size();
    }

    /**
     * Stop the service and release used resources.
     */
    @Override
    public void close() {
        task.interrupt();
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        isClosed = true;
    }
}
