package semistreamSimilarityJoin;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.ceil;

/**
 * {code ParallelDiskBuffer} is preferable than this class. This is a supporting class for {code ParallelDiskBuffer}
 */
public class DiskBuffer implements Buffer<String>{
    private BufferedReader reader;
    private FileInputStream inputStream;
    private int split;
    private long batchSize;
    private boolean isClosed;

    public DiskBuffer(String path, int split) throws IOException {
        this(new File(path), split);
    }

    public DiskBuffer(File  file, int split) throws IOException {
        inputStream = new FileInputStream(file);
        resetReader();
        this.split = split;
        setBatchSize(reader);
        isClosed = false;
    }


    @Override
    public List<Pair<String, String>> read() throws IOException {
        List<Pair<String, String>> tuples = new ArrayList<>(split);
        if (isClosed) {
            throw new IOException("Try to read from the disk Buffer that has been closed");
        }
        for(int i = 0; reader.ready() && i < batchSize; i++){
            String word = reader.readLine();
            tuples.add(new Pair<String, String>(word, word));
        }
        if (!reader.ready()) { //It has finished once traverse.
            resetReader();
        }
        return tuples;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
        reader.close();
        isClosed = true;
    }

    @Override
    public int getSplit() {
        return split;
    }

    private void setBatchSize(BufferedReader reader) throws IOException {
        long number = reader.lines().count();
        resetReader();
        batchSize = (long)ceil((double)number / split);
    }

    private void resetReader() throws IOException {
        inputStream.getChannel().position(0);
        reader = new BufferedReader(new InputStreamReader(inputStream));
    }
}
