package semistreamSimilarityJoin;

import java.util.ArrayList;
import java.util.List;

/**
 * Utilities for segmenting strings
 */
public class Segmentor {

    /**
     * Evenly split a work into <tt>num</tt> partitions.
     */
    public static List<IndexKey> segmentEvenly(final String word, int num) {
        List<IndexKey> indices = new ArrayList<>();
        final int step = (int)Math.ceil((double)word.length()/num);
        int shortNum = step * num - word.length();

        int size = 0;
        for (int i = 0; i < word.length();) {
            if (shortNum > 0) {
                size = step - 1;
                shortNum -= 1;
            } else {
                size = step;
            }
            if (size != 0) {
                indices.add(new IndexKey(word.length(), indices.size(), word.substring(i, i + size)));
            }
            i += size;
        }
        return indices;
    }

    /**
     * Based on multi-match-aware filtering, enumerate all the possible segments
     * @param length the length of the words in comparison.
     * @return
     */
    public static List<IndexKey> segmentAllPairs(String word, final int num, final int length) {
        List<IndexKey> indices = new ArrayList<>();
        final int step = (int)Math.ceil((double)length/num);
        int shortNum = step * num - length;
        final int lengthDifference = Math.abs(word.length() - length);
        int size = 0;
        int order = 0;
        for (int i = 0; i < word.length();) {
            if (shortNum > 0) {
                size = step - 1;
                shortNum -= 1;
            } else {
                size = step;
            }
            if (size == 0) {
                break;
            }
            addIndices(num - 1, indices, word, length, lengthDifference, order, i, size);
            i += size;
            order += 1;
        }

        return indices;
    }

    private static void addIndices(final int distance, List<IndexKey> indices, String word, final int length,
                                   final int lengthDifference, final int order, final int pos, final int size) {
        int left_begin = Math.max(0, pos - order);
        int left_end = Math.min(word.length() - size, pos + order);
        int right_begin = Math.max(0, pos + lengthDifference - distance + order);
        int right_end = Math.min(word.length() - size, pos + lengthDifference + distance - order);

        int begin = Math.max(left_begin, right_begin);
        int end = Math.min(left_end, right_end);
        for (int i = begin; i <= end; i++) {
            indices.add(new IndexKey(length, order, word.substring(i, i + size)));
        }
    }

}
