package semistreamSimilarityJoin;


import java.io.IOException;
import java.util.List;

/**
 * The abstraction of stream in the algorithm. The stream can be infinite or finite.
 * @param <V> other values in a stream tuple except the key.
 */
public interface Stream <V> {
    /**
     * Open a stream and make sure it is ready to serve.
     */
    void open() throws IOException;

    /**
     * Read all the tuples available. This method is asynchronous and return the available data instantly.
     * @return a list of tuples including key and other information in a tuple.
     */
    List<Pair<String, V>> read() throws IOException;

    /**
     * Read at most n tuples available. This method may throw {code UncheckedIOException}
     * @param n maximum number of tuples returned.
     * @return a list of tuples including key and other information in a tuple.
     */
    List<Pair<String, V>> read(int n) throws IOException;

    /**
     * Test whether there are tuples ready to read.
     * @return if there is available tuples in the stream return true, otherwise return false.
     */
    boolean available();

    /**
     * Test whether the steam is able to generate more tuples. If this method return false, the
     * available() method may return true, because some tuples may be stored in the stream's buffer.
     * @return
     */
    boolean isAlive();

    /**
     * Return the number of available tuples in the stream at the time of this method called.
     * The following read() may get more tuples as new tuples may come in the stream during
     * the interval of the two methods.
     * @return the number of available tuples in the stream.
     */
    int size();

    /**
     * Stop the service and release used resources.
     */
    void close();
}
