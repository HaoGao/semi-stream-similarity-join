package semistreamSimilarityJoin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


/**
 * A wrapper of {code DiskBuffer}, which enables pre-load. Compared with DiskBuffer, 
 * this class pre-loads disk tuples into a buffer to reduce the waiting time. Considering the amount of master data
 * read in one loop, the waiting time reduced is considerable.
 */
public class ParallelDiskBuffer implements Buffer<String> {
    private final int split;
    private final DiskBuffer diskBuffer;
    private List<Pair<String, String>> diskTuples;
    
    //read tuples using diskBuffer
    private final Thread task;

    //lock for accessing the container(empty or full) diskTuples
    private final ReentrantLock diskTupleLock = new ReentrantLock();
    
    //signal diskTuples is empty
    private final Condition emptyDiskTuples = diskTupleLock.newCondition();
    //signal diskTuples is full
    private final Condition fullDiskTuples = diskTupleLock.newCondition();

    public ParallelDiskBuffer(String path, int split) throws IOException {
        this(new File(path), split);
    }

    public ParallelDiskBuffer(File file, int split) throws IOException {
        this.split = split;
        diskBuffer = new DiskBuffer(file, split);
        task = new Thread(this::service);
        task.start();
    }
    
    @Override
    public List<Pair<String, String>> read() throws IOException {
        List<Pair<String, String>> tuples;
        diskTupleLock.lock();
        try {
            while (diskTuples == null) {
                fullDiskTuples.await();
            }
            tuples = diskTuples;
            diskTuples = null;  //indicate diskTuples is empty
            emptyDiskTuples.signal();
        } catch (InterruptedException e) {
            throw new IOException(e);  //satisfy the interface protocol
        } finally {
            diskTupleLock.unlock();
        }
        return tuples;
    }
    
    private void service()  {
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                System.err.println("ParallelDiskBuffer has been interrupted");
                return;
            }
            diskTupleLock.lock();
            try {
                while (diskTuples != null) {
                    emptyDiskTuples.await();
                }
                diskTuples = diskBuffer.read();
                fullDiskTuples.signal();

            } catch (InterruptedException | IOException e) {
                System.err.println("ParallelDiskBuffer exited");
                return;
            } finally {
                diskTupleLock.unlock();
            }
        }
    }

    @Override
    public void close() throws IOException {
        diskBuffer.close();
        task.interrupt();
    }

    @Override
    public int getSplit() {
        return split;
    }
}
