package semistreamSimilarityJoin;

import java.io.IOException;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Created by life on 15/12/14.
 */
public class TwitterWordSemiStreamJoin {
    public static void main(String[] args) throws IOException {
        Stream<String> stream = new TwitterWordStream();
        Buffer<String> buffer = new DiskBuffer("words.txt", 3);
        SimilarityJoin<String> similarityJoin = new TrieJoin<>(buffer.getSplit());
        BiConsumer<Pair<String, String>, List<MatchResult<String>>> consumer =
                (diskTuple, matchResults) -> {
                    if (!matchResults.isEmpty()) {
                        System.out.println(diskTuple + ":" + matchResults);
                    }
                };
        ParallelSemiStreamJoin<String, String> semiStreamJoin = new ParallelSemiStreamJoin.Builder<String, String>()
                .setStream(stream)
                .setSimilarityJoin(similarityJoin)
                .setBuffer(buffer)
                .setConsumer(consumer)
                .setMaxDistance(1)
                .build();
        semiStreamJoin.start();
    }
}
