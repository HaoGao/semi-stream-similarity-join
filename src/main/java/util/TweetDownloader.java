package util;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This program fetches random English tweets from twitter.com using twitter stream, and then store them into a database.
 * For every tweet, Id, author, time and text are stored.
 * The connection parameters are set below:
 * url: localhost: 3306
 * database: twitter 
 * user: lab
 * password: streamjoin 
 */
public class TweetDownloader {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //set database
        Class.forName("com.mysql.jdbc.Driver");
        // setup the connection with the DB.
        Connection connect = DriverManager
                .getConnection("jdbc:mysql://localhost/twitter?"
                        + "user=lab&password=streamjoin");
        PreparedStatement preparedStatement = connect
                .prepareStatement("insert into  twitter.tweet values (?, ? , ?, ?)");
        
        
        //set random tweets stream
        TwitterStream twitterStream;
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("P1i8YwpHMhQQnwGFPSHw3XzJ9")
                .setOAuthConsumerSecret("Jdhc85wZiWTSI0s0THCHAOo2UIgNKey5INYF6KFzDSVjVcgowS")
                .setOAuthAccessToken("2623222722-oLv4KnfqUFfM0LBnrgIHthCPZiy7JNhmeXsApnS")
                .setOAuthAccessTokenSecret("nJFdybjno19WrLCFuOnc0AHyC6HekjspwquBFKbPfCdsV");
        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        StatusListener listener = new StatusListener() {
            @Override
            public void onStatus(Status status) {
                if (!isSpanish(status.getText()) && isEnglish(status.getText())) {
//                    System.out.println(status.getText() + ":" + status.getUser().getScreenName() + ":" + status.getId()
//                    + ":" + status.getCreatedAt());
                    try {
                        preparedStatement.setLong(1, status.getId());
                        preparedStatement.setDate(2, new java.sql.Date(status.getCreatedAt().getTime()));
                        preparedStatement.setString(3, status.getUser().getName());
                        preparedStatement.setString(4, status.getText());
                        preparedStatement.executeUpdate();
                    } catch (SQLException e) {  //Exceptions may caused by incompatible charset. 
 //                       e.printStackTrace();
                    }

                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                //empty
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.err.println("[Twitter]Got track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                //empty
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                //empty
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);
        twitterStream.sample();
    }
    
    private static boolean isEnglish(String text) {
        String[] words = text.split("\\s+");
        long counter = Arrays.asList(words).stream().filter(TweetDownloader::isCharacters).count();
        return counter > 10;
    }
    


    private static boolean isCharacters(String word) {
        return word.toLowerCase().chars().allMatch(c -> c >= 'a' && c <= 'z' || c >= '0' && c <= '9');
    }

    /**
     * simply filter for filter Spanish tweets.
     */
    private static boolean isSpanish(String text) {
        Character[] spanish = {
                'á', 'é', 'í', 'ó', 'ú', 'ü', 'ñ', '¿', '¡', 'ã', };
        Set<Character> spanishCharacters = new HashSet<>((Arrays.asList(spanish)));
        //caution: string's chars return an intStream rather than a charStream (in fact, there is no charStream available)
        return text.toLowerCase().chars().anyMatch(c -> spanishCharacters.contains((char)c));
    }
}
