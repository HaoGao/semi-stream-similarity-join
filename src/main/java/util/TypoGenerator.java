package util;


import semistreamSimilarityJoin.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class TypoGenerator {
    private Random random;
    List<String> words = new ArrayList<>();
    private int modifications;

    public TypoGenerator(String wordPath, int modifications) throws IOException {
        this.random = new Random();
        this.modifications = modifications;
        readWords(wordPath);
    }

    private void readWords(String path) throws IOException {
        BufferedReader reader = Files.newBufferedReader(Paths.get(path));
        while (reader.ready()) {
            String word = reader.readLine().trim();
            if (!word.isEmpty()) {
                words.add(word);
            }
        }
        reader.close();
    }

    /**
     * Generate a pair of manual typo and the original right word. At most three times of modification can be applied to
     * one word.
     * @return a pair of typo and the original right word.
     */
    public Pair<String, String> generate() {
        final String word = words.get(randomInt(0, words.size()));
        int modifyTimes = randomInt(0, word.length());
        modifyTimes = modifyTimes < modifications ? modifyTimes : modifications;  //limit the modification times
        String typo = word;
        for (int i = 0; i < modifyTimes; i++) {
            typo = modify(typo);
        }
        return new Pair<String, String>(typo, word);
    }

    private String modify(String word) {
        int type = randomInt(0, 4);
        String typo;
        int pos = randomInt(0, word.length());

        switch (type) {
            case 0:  //replace
                typo = word.substring(0, pos) + randomChar() + word.substring(pos + 1);
                break;
            case 1:  //delete
                typo = word.substring(0, pos) + word.substring(pos + 1);
                break;
            case 2:  //insert
                typo = word.substring(0, pos) + randomChar() + word.substring(pos);
                break;
            default:  //unchanged
                typo = word;
        }
        return typo;
    }

    /**
     * Generate an integer equal to or larger than the lower bound <tt>from</tt> and less than the upper bound <tt>to</tt>
     * @param from the lower bound
     * @param to the upper bound
     * @return an integer between <tt>from</tt>, inclusive, and <tt>to</tt>, exclusive.
     */
    private int randomInt(int from, int to) {
        if (to < from) {
            throw new IllegalArgumentException("The upper bound should be larger than the lower bound");
        }
        float v = random.nextFloat();
        return (int)((to - from) * v) + from;
    }

    private char randomChar() {
        final String characters = "abcdefghijklmnopqrstuvwxyz";
        return characters.charAt(randomInt(0, characters.length()));
    }
}
