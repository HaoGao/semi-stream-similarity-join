package util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by life on 10/12/14.
 */
public class Util {
    public static String join(Collection<?> list, String separator) {
        if (list == null || list.isEmpty()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (Object v: list) {
            builder.append(v.toString() + separator);
        }
        return builder.substring(0, builder.length() - separator.length());
    }

    public static String join(Object[] objects, String separator) {
        return join(Arrays.asList(objects), separator);
    }

    public static String timestampString() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    }
    
    public static String getContentOfStackTrace(Throwable e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }
}
