package semistreamSimilarityJoin;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditDistanceCalculatorTest {

    @Test
    public void testIsSimilar() {
        EditDistanceCalculator tool = new EditDistanceCalculator(32);
        assertFalse(tool.isSimilar("deponing", "rptuning", 1));
        assertFalse(tool.isSimilar("cotcan", "cotans", 1));
        assertTrue(tool.isSimilar("abcdef", "abcdef", 0));
        assertTrue(tool.isSimilar("abcdef", "abcdef", 1));
        assertTrue(tool.isSimilar("abcdef", "abcdef", 2));

        //deletion or insertion, true
        assertTrue(tool.isSimilar("abcdef", "abdef", 1));
        assertTrue(tool.isSimilar("abef", "abcdef", 2));
        assertTrue(tool.isSimilar("abf", "abcdef", 3));

        //deletion false
        assertFalse(tool.isSimilar("abcdef", "abdef", 0));
        assertFalse(tool.isSimilar("abef", "abcdef", 1));
        assertFalse(tool.isSimilar("abf", "abcdef", 2));

        //replacement true
        assertTrue(tool.isSimilar("abcdef", "abddef", 1));
        assertTrue(tool.isSimilar("abeeef", "abcdef", 2));
        assertTrue(tool.isSimilar("ab456f", "abcdef", 3));

        //replacement false
        assertFalse(tool.isSimilar("abcdef", "abddef", 0));
        assertFalse(tool.isSimilar("abeeef", "abcdef", 1));
        assertFalse(tool.isSimilar("ab456f", "abcdef", 2));

        //extreme conditions
        assertFalse(tool.isSimilar("", "abddef", 1));
        assertTrue(tool.isSimilar("", "", 0));
        assertTrue(tool.isSimilar("ab4", "", 3));
    }

}
