package semistreamSimilarityJoin;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;


public class SegmentorTest {
    @Test
    public void testSegmentEvenly(){
        String s = "passings";
        List<IndexKey> indices = Segmentor.segmentEvenly(s, 4);
        IndexKey[] expecteds = {
                new IndexKey(8, 0, "pa"),
                new IndexKey(8, 1, "ss"),
                new IndexKey(8, 2, "in"),
                new IndexKey(8, 3, "gs"),
        };
        assertArrayEquals(expecteds,indices.toArray());



        String s1 = "the";
        IndexKey[] expecteds1 = {
                new IndexKey(3, 0, "t"),
                new IndexKey(3, 1, "h"),
                new IndexKey(3, 2, "e"),
        };
        List<IndexKey> indices1 = Segmentor.segmentEvenly(s1, 4);
        assertArrayEquals(expecteds1, indices1.toArray());
    }

    @Test
    public void testSegmentAllPairs(){
        String s = "passings";

        List<IndexKey> allIndices = Segmentor.segmentAllPairs("adjoint", 4, s.length());
        IndexKey[] expecteds = {
                new IndexKey(8, 0, "ad"),
                new IndexKey(8, 1, "dj"),
                new IndexKey(8, 1, "jo"),
                new IndexKey(8, 1, "oi"),
                new IndexKey(8, 2, "in"),
                new IndexKey(8, 2, "nt"),
        };
        assertArrayEquals(expecteds, allIndices.toArray());

    }
}
