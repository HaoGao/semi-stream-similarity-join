package semistreamSimilarityJoin;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PassJoinIndexTest {

    @Test
    public void testPut() {
        final String words [] = new String [] {
                "hello",
                "hello1",
                "hello2",
                "thank",
                "think",
                "tank",
                "sparrow3",
                "sparrow5",
                "sparrow4",
                "sparrow3",
        };
        PassJoinIndex index = new PassJoinIndex(2);
        for (int i = 0; i < words.length; i++) {
            index.put(words[i]);
            assertEquals(i + 1, index.size());
        }
    }

    @Test
    public void testGet() {
        final String words [] = new String [] {
                "hello",  //0
                "hello1",  //1
                "hello2",  //2
                "thank",  //3
                "think",  //4
                "tank",  //5
                "sparrow3",  //6
                "sparrow5",  //7
                "sparrow4",  //8
                "sparrow3",  //9
        };
        PassJoinIndex index = new PassJoinIndex(2);
        for (int i = 0; i < words.length; i++) {
            index.put(words[i]);
        }

//        for (int i = 0; i < words.length; i++) {
//            assertTrue(index.get(words[i], 2).contains(words[i]));
//        }

        System.err.println(index.get(words[0], 2));
        assertTrue(index.get(words[0], 2).contains(words[1]));
        assertTrue(index.get(words[1], 2).contains(words[2]));
        assertFalse(index.get(words[2], 2).contains(words[3]));
        assertTrue(index.get(words[3], 2).contains(words[4]));
        assertFalse(index.get(words[4], 2).contains(words[5]));
        assertFalse(index.get(words[5], 2).contains(words[6]));
        assertTrue(index.get(words[6], 2).contains(words[7]));
        assertTrue(index.get(words[7], 2).contains(words[8]));
        assertTrue(index.get(words[8], 2).contains(words[9]));
    }
}
