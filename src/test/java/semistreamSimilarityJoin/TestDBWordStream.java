package semistreamSimilarityJoin;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test data stream from databases.
 * NOTICE: please configure database connection to enable the test.
 */
@Ignore
public class TestDBWordStream {
    
    private String url="jdbc:mysql://localhost:3306/twitter";
    private String user="lab";
    private String password = "streamjoin";
    @Test
    public void testIsAlive() throws SQLException, ClassNotFoundException, IOException {
        DBWordStream stream = new DBWordStream(url, user, password, 1000);
        stream.open();
        assertTrue(stream.isAlive());
    }


    @Test
    public void testAvailable() throws SQLException, ClassNotFoundException, InterruptedException, IOException {
        DBWordStream stream = new DBWordStream(url, user, password, 0);
        stream.open();
        Thread.sleep(1000);
        assertTrue(stream.available());
    }


    @Test
    public void testRead() throws SQLException, ClassNotFoundException, InterruptedException, IOException {
        DBWordStream stream = new DBWordStream(url, user, password, 0);
        stream.open();
        Thread.sleep(1000);
        List<Pair<String, String>> tuples = stream.read();
        assertFalse(tuples.isEmpty());
    }
}
