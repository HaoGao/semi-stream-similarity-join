package semistreamSimilarityJoin;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestDiskBuffer {
    @Test
    public void testRead() throws IOException {
        System.err.println(getClass().getResource("/words.txt"));
        final String wordPath = getClass().getResource("/words.txt").getPath();
        final String wordFile = System.getProperty("os.name").contains("indow") ? wordPath.substring(1) : wordPath;
        Buffer<String> buffer = new DiskBuffer(wordFile, 1);
        List<Pair<String, String>> list = buffer.read();
        long lineNumber = Files.newBufferedReader(Paths.get(wordFile)).lines().count();
        assertEquals(lineNumber, list.size());

        buffer.close();

        buffer = new DiskBuffer(wordFile, 3);
        List<Pair<String, String>> list1 = buffer.read();
        List<Pair<String, String>> list2 = buffer.read();
        List<Pair<String, String>> list3 = buffer.read();
        assertEquals(list1.size(), list2.size());
        assertTrue(list3.size() <= list2.size());

        List<Pair<String, String>> list4 = buffer.read();
        List<Pair<String, String>> list5 = buffer.read();
        List<Pair<String, String>> list6 = buffer.read();
        assertArrayEquals(list1.toArray(), list4.toArray());
        assertArrayEquals(list2.toArray(), list5.toArray());
        assertArrayEquals(list3.toArray(), list6.toArray());

        buffer.close();


        buffer = new DiskBuffer(wordFile, 4);
        list1 = buffer.read();
        list2 = buffer.read();
        list3 = buffer.read();
        list4 = buffer.read();
        assertEquals(list1.size(), list2.size());
        assertEquals(list2.size(), list3.size());
        assertTrue(list4.size() <= list3.size());

        list5 = buffer.read();
        list6 = buffer.read();
        List<Pair<String, String>> list7 = buffer.read();
        List<Pair<String, String>> list8 = buffer.read();
        assertArrayEquals(list1.toArray(), list5.toArray());
        assertArrayEquals(list2.toArray(), list6.toArray());
        assertArrayEquals(list3.toArray(), list7.toArray());
        assertArrayEquals(list4.toArray(), list8.toArray());

        buffer.close();
    }
}
