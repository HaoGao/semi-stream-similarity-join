package semistreamSimilarityJoin;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IndexKeyTest {

    @Test
    public void testCompareTo() {
        IndexKey[] keys = {
                new IndexKey(2, 3, "hello"),
                new IndexKey(3, 3, "hello"),
                new IndexKey(3, 4, "hello"),
                new IndexKey(3, 3, "apple"),
                new IndexKey(3, 3, "bingo"),
                new IndexKey(3, 3, "bingo"),
                new IndexKey(3, 3, ""),
                new IndexKey(3, 3, ""),

        };
        assertTrue(keys[0].compareTo(keys[1]) < 0);
        assertTrue(keys[1].compareTo(keys[0]) > 0);
        assertTrue(keys[1].compareTo(keys[2]) < 0);
        assertTrue(keys[2].compareTo(keys[3]) > 0);
        assertTrue(keys[3].compareTo(keys[4]) < 0);
        assertTrue(keys[4].compareTo(keys[5]) == 0);
        assertTrue(keys[5].compareTo(keys[6]) > 0);
        assertTrue(keys[6].compareTo(keys[7]) == 0);

    }

    @Test
    public void testEquals() {
        IndexKey[] keys = {
                new IndexKey(2, 3, "hello"), //0
                new IndexKey(3, 3, "hello"), //1
                new IndexKey(3, 4, "hello"), //2
                new IndexKey(3, 3, "apple"), //3
                new IndexKey(3, 3, "bingo"), //4
                new IndexKey(3, 3, "bingo"),  //5
                new IndexKey(3, 3, ""),  //6
                new IndexKey(3, 3, ""),  //7
        };
        assertFalse(keys[0].equals(keys[1]));
        assertFalse(keys[1].equals(keys[0]));
        assertFalse(keys[1].equals(keys[2]));
        assertFalse(keys[2].equals(keys[3]));
        assertFalse(keys[3].equals(keys[4]));
        assertTrue(keys[4].equals(keys[5]));
        assertFalse(keys[5].equals(keys[6]));
        assertTrue(keys[6].equals(keys[7]));
    }
}
