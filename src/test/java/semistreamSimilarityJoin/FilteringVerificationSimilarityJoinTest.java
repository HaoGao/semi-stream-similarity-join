package semistreamSimilarityJoin;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FilteringVerificationSimilarityJoinTest {
    private Path wordListPath;

    {
        try {
            wordListPath = Paths.get(getClass().getResource("/words.txt").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    private final String testWords [] = new String [] {
            "hllo", //1
            "hbello", //1
            "hxllo", //2
            "thank", //0
            "thxnk", //2
            "tank", //0
            "sparrow", //0
            "sparow", //1
            "sperrow", //2
            "spparrow",//1
    };

    public final String similarWords [] = new String [] {
            "hello",
            "hello",
            "hello",
            "thank",
            "think",
            "tank",
            "sparrow",
            "sparrow",
            "sparrow",
            "sparrow",
    };

    public final String removedWords [] = new String[] {
            "hello",
            "thank",
            "think",
            "tank",
            "sparrow",
    };

    public SimilarityJoin<String> getDict() {
        Index[] indices = new Index[5];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = new PassJoinIndex(3);
        }
        SimilarityJoin<String> dict = new FilteringVerificationSimilarityJoin<>(indices, 3, 30);
        try {
            BufferedReader reader = Files.newBufferedReader(wordListPath);
            while (reader.ready()) {
                String line = reader.readLine();
                dict.put(line, line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dict;
    }

    public SimilarityJoin<String> getEmptyDict() {
        Index[] indices = new Index[5];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = new PassJoinIndex(3);
        }
        SimilarityJoin<String> dict = new FilteringVerificationSimilarityJoin<>(indices, 3, 30);
        return dict;
    }

    @Test
    public void testSize() {
        SimilarityJoin dict = getDict();
        assertTrue(dict.size() > 0);
    }

    @Test
    public void testPrune() {
        List<String> words = new ArrayList<>();
        try {
            BufferedReader reader = Files.newBufferedReader(wordListPath);
            while (reader.ready()) {
                String line = reader.readLine();
                words.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //test the situation when the split is 1
        SimilarityJoin<String> dict = getEmptyDict();
        for(int i = 0; i < 3000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        assertEquals(3000, dict.size());
        dict.prune();
        assertEquals(3000, dict.size());
        dict.prune();
        assertEquals(3000, dict.size());
        dict.prune();
        assertEquals(3000, dict.size());
        dict.prune();
        assertEquals(3000, dict.size());
        dict.prune();
        assertEquals(0, dict.size());
    }
}
