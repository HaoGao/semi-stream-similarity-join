package semistreamSimilarityJoin;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;


@Ignore
public class TrieJoinTest {
    private final Path wordListPath = Paths.get(getClass().getResource("/words.txt").getPath());
    private final String testWords [] = new String [] {
            "hllo", //1
            "hbello", //1
            "hxllo", //2
            "thank", //0
            "thxnk", //2
            "tank", //0
            "sparrow", //0
            "sparow", //1
            "sperrow", //2
            "spparrow",//1
    };

    public final String similarWords [] = new String [] {
            "hello",
            "hello",
            "hello",
            "thank",
            "think",
            "tank",
            "sparrow",
            "sparrow",
            "sparrow",
            "sparrow",
    };

    public final String removedWords [] = new String[] {
            "hello",
            "thank",
            "think",
            "tank",
            "sparrow",
    };

    public SimilarityJoin<String> getDict() {
        SimilarityJoin<String> dict = new TrieJoin<>(3);
        try {
            BufferedReader reader = Files.newBufferedReader(wordListPath);
            while (reader.ready()) {
                String line = reader.readLine();
                dict.put(line, line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dict;
    }

    @Test
    public void testSize() {
        SimilarityJoin dict = getDict();
        assertTrue(dict.size() > 0);
    }

    @Test
    public void testGet() {
        SimilarityJoin<String> dict = getDict();

        for(String word: similarWords) {
            List<MatchResult<String>> r = dict.get(word, 0);
            assertEquals("Exact match of " + word + "should return only one element", 1, r.size());
            assertEquals(1, r.get(0).getValues().size());
            assertEquals("Exact match should return the same word", r.get(0).getKey(), word);
        }

        for (int i = 0; i < testWords.length; i++) {
            String testWord = testWords[i];
            String similarWord = similarWords[i];
            List<MatchResult<String>> results = dict.get(testWord, 2);
            assertTrue(
                    results.stream().
                            anyMatch(
                                    r -> r.getKey().equals(similarWord))
            );
        }
    }

    @Test
    public void testPrune() {
        List<String> words = new ArrayList<>();
        try {
            BufferedReader reader = Files.newBufferedReader(wordListPath);
            while (reader.ready()) {
                String line = reader.readLine();
                words.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //test the situation when the split is 1
        SimilarityJoin<String> dict = new TrieJoin<>(1);
        for(int i = 0; i < 3000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        dict.prune();
        for(int i = 0; i < 3000; i++) {
            assertTrue(dict.get(words.get(i), 0).isEmpty());
        }

        for(int i = 3000; i < 6000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        for (int i = 3000; i < 6000; i++) {
            List<MatchResult<String>> r = dict.get(words.get(i), 0);
            assertEquals(1, r.size());
            assertEquals(words.get(i), r.get(0).getKey());
        }

        dict.prune();

        for(int i = 3000; i < 6000; i++) {
            assertTrue(dict.get(words.get(i), 0).isEmpty());
        }

        //test the situation when the split is 3.
        dict = new TrieJoin<>(3);
        for( int i = 0; i < 3000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        dict.prune();
        for (int i = 0; i < 3000; i++) {
            List<MatchResult<String>> r = dict.get(words.get(i), 0);
            assertEquals(1, r.size());
            assertEquals(words.get(i), r.get(0).getKey());
        }
        for( int i = 3000; i < 6000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        dict.prune();
        for (int i = 3000; i < 6000; i++) {
            List<MatchResult<String>> r = dict.get(words.get(i), 0);
            assertEquals(1, r.size());
            assertEquals(words.get(i), r.get(0).getKey());
        }

        for( int i = 6000; i < 9000; i++) {
            dict.put(words.get(i), words.get(i));
        }
        dict.prune();
        for(int i = 0; i < 3000; i++) {
            assertTrue(dict.get(words.get(i), 0).isEmpty());
        }

        for (int i = 3000; i < 6000; i++) {
            List<MatchResult<String>> r = dict.get(words.get(i), 0);
            assertEquals(1, r.size());
            assertEquals(words.get(i), r.get(0).getKey());
        }



    }
}
