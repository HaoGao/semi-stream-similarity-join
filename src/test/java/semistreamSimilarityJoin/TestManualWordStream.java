package semistreamSimilarityJoin;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestManualWordStream {
    private final String wordPath = getClass().getResource("/words.txt").getPath();
    private final String wordFile = System.getProperty("os.name").contains("indow") ? wordPath.substring(1) : wordPath;


    @Test
    public void testOpen() throws IOException {
        Stream<String> stream = new ManualWordStream(100, 1, wordFile);
        assertFalse(stream.isAlive());
        stream.open();
        assertTrue(stream.isAlive());
        stream.close();
    }

    @Test
    public void testRead() throws InterruptedException, IOException {
        Stream<String> stream = new ManualWordStream(100, 1, wordFile);
        stream.open();
        Thread.sleep(100);
        List<Pair<String, String>> list = stream.read();
        assertFalse(list.isEmpty());
        stream.close();

        stream = new ManualWordStream(100, 1, wordFile);
        stream.open();
        Thread.sleep(200);
        list = stream.read();
        assertFalse(list.isEmpty());
        stream.close();
    }

    @Test
    public void testSize() throws InterruptedException, IOException {
        Stream<String> stream = new ManualWordStream(100, 1, wordFile);
        stream.open();
        Thread.sleep(100);
        List<Pair<String, String>> list = stream.read();
        assertTrue(list.size() > 0);
        stream.close();
    }

    @Test
    public void testAvailable() throws InterruptedException, IOException {
        Stream<String> stream = new ManualWordStream(100, 1, wordFile);
        assertFalse("Stream should be not available before open.", stream.available());
        stream.open();
        Thread.sleep(100);
        assertTrue(stream.available());
        stream.close();
    }
}
