#!/bin/bash
java -Xmx7G -cp  target/semistreamSimilarityJoin-1.0-SNAPSHOT-jar-with-dependencies.jar \
semistreamSimilarityJoin.SemiStreamJoinMeasurement \
 --autoFlushStatisticResult=true \
 --speed=500 \
 --trieType=3\
 --joinThreadNumber=4\
 --outputResults=false

